import React, { PureComponent } from 'react';
import './App.css';
import CheckBox from './CheckBox'
import SimClick from './SimClick'

interface AppState {
  foo: string;
}

export default class App extends PureComponent<{}, AppState> {

  state = {
    foo: "parent's state"
  };

  render(): React.ReactNode {
    return (
      <div className="App">
        <CheckBox foo = {this.state.foo} />
        <SimClick />
      </div>
    );
  }
} 
