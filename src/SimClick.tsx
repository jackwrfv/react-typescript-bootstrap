import React, { PureComponent } from "react";


class SimClick extends PureComponent<{}, {}> {



    componentDidMount() {
        setTimeout(() => {
            let btn = document.querySelector("button[id=my]");
            this.simulateMouseClick(btn);
        }, 3000);
    }


    simulateMouseClick(el: any) {
        let opts = {bubbles: true};
        el.dispatchEvent(new MouseEvent("click",opts));
    }

    render() {
        return (
            <>
                <button id="my" onClick={() => console.log("uityiyuity")}>
                    click me
                </button>
            </>
        );
    }
}

export default SimClick;
